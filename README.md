# berlin web service

### POST requests to
- /vote    Place the votes
- /Record  Gather up the votes and pick one of the entries

### Get requests to
- /votes    Get todays votes
- /allvotes Show all votes ever
- /results  Get the result from today's vote (if decided)

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## TODO
No current TODO list

## Running

### Remote Running
Stand up a Linux server  
Sample steps for AWS:

#### Set up the AWS instance 
- Create the EC2 instance from the aws console
- Create & download new key pair
- Use the PEM file for SSH access
- set up the allow traffic rule in security groups
  - Copy an existing rule
  - allow Custom TCP rule incoming on port 3000
  - change the machine security group to the one just set up (allowing TCP port 3000)

#### SSH to the server
- CD to the directory with the PEM file
- chmod 400 <PEMFileName>
- ssh to the server using the command from the aws console  
  eg. `ssh -i "coffeeVoter.pem" ubuntu@ec2-54-252-140-130.ap-southeast-2.compute.amazonaws.com`

#### Set up the server
- sudo passwd **then enter the new root password**
- `sudo apt-get install git`
- `sudo apt-get update`
- create a `code` directory
- install leiningen : `sudo apt-get install leiningen` or see leiningen.org (download the lein file and put it in your path)
  - `wget https://raw.github.com/technomancy/leiningen/stable/bin/lein -O ~/bin/lein`
  - `chmod a+x ~/bin/lein`
- install java `sudo apt-get install default-jre`

#### Set up the application
- `sudo git clone https://Jonathanheaden@bitbucket.org/Jonathanheaden/berlin.git` 
- run the app to make sure dependencies are downloaded `lein ring server` from the berlin directory
- create the uberjar `lein ring uberjar`
- create an upstart daemon to run the service:
  - create a file called `/etc/init/berlin.conf`
  - add the following contents
    >## Upstart config file (use 'start berlin', 'stop berlin')
     ## stdout and stderr will be captured in /var/log/upstart/berlin.log
     author "Jonathan Headen"
     description "Berlin Web Service"
     start on (local-filesystems and net-device-up IFACE!=lo)
     exec java -jar /srv/berlin/berlin-0.1.0-SNAPSHOT-standalone.jar
     ## Try to restart up to 10 times within 5 min:
     respawn limit 10 300

- copy the uberjar to the directory
- start the daemon
  - `start berlin`

Note the EC2 IP address from the AWS console.  
The url for reading votes will be `http://<IP ADDRESS>:3000/votes`
The url for posting will be `http://<IP ADDRESS:3000/vote`

### Local testing
To start a web server for the application, run:

    lein ring server

## Client connections

Set up IFTTT Do It Button
- Configure the Maker channel to push to the host url  


## License

Copyright © 2016 FIXME
