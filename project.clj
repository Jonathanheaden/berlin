(defproject berlin "0.1.0-SNAPSHOT"
  :description "Berlin web service"
  :url "http://coffee.org"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [compojure "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [hiccup "1.0.5"]
                 [ring-server "0.3.1"]
                 [liberator "0.14.1"]
                 [cheshire "5.3.1"]
                 [lib-noir "0.8.2"]
                 [clj-http "2.1.0"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler berlin.handler/app
         :init berlin.handler/init
         :destroy berlin.handler/destroy
         }
  :profiles
  {:uberjar {:aot :all}
   :production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}} 
   :dev
   {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})

