(ns berlin.routes.home
  (:require [hiccup.element :refer [link-to]]
            [ring.util.response :as response]
            [compojure.core :refer :all]
            [berlin.views.layout :as layout]            
            [liberator.core
             :refer [defresource resource request-method-in]]
            [cheshire.core :refer [generate-string]]
            [noir.io :as io]
            [clojure.java.io :refer [file]]
            [berlin.models.data :as data]
            ))

(defresource home
:handle-ok "Berlin Web Service!"
:etag "fixed-etag" :available-media-types ["text/plain"])

(defresource add-vote
  :available-media-types ["text/html"]

  :exists?
  (fn [context]
    [(io/get-resource "/newvote.html" )
     {::file (file (str (io/resource-path) "/newvote.html"))}])

  :handle-ok
  (fn [{{{resource :resource} :route-params} :request}]
    (clojure.java.io/input-stream (io/get-resource "/newvote.html")))

  :last-modified
  (fn [{{{resource :resource} :route-params} :request}]
    (.lastModified (file (str (io/resource-path) "/newvote.html")))))

(defresource get-votes
  :allowed-methods [:get]
  :handle-ok (fn [_]  (let [votes (atom (data/all-todays-votes))]
                        (generate-string @votes)))
  :available-media-types ["application/json"])

(defresource get-todaysvotes
  :allowed-methods [:get]
  :handle-ok (fn[_] (let [result (atom (data/results))]
                      (generate-string @result)))
  :available-media-types ["application/json"])

(defresource submit-vote [person shop]
  :allowed-methods [:get :post]
  :malformed? (fn [context]
                (let [params (get-in context [:request :form-params])]
                  (or (empty? (get params "person"))
                      (empty? (get params "shop"))
                   ))
                )
  :handle-malformed "Shop and Person must be submitted!"
  :post!  (data/add-newvote person shop)
  :handle-created "OK"
  :post-redirect? {:location home}
  :handle-ok {:location get-votes }
  :handle-error "Error occurred"
  :available-media-types ["application/json"])

(defresource record-result
  :allowed-methods [:post]
  :post! (fn [_] (data/record-votes))
  :handle-created "OK"
  :post-redirect? {:location home}
  :handle-ok {:location get-votes }
  :handle-error "Error occurred"
  :available-media-types ["application/json"])

(defroutes home-routes
  (GET "/" request home)
  (GET "/votes" request get-votes)
  (GET "/results" request get-todaysvotes)
  (GET "/newvote" request add-vote)
  (ANY "/vote" [person shop] (submit-vote person shop))
  (ANY "/record" request record-result)
  )
