(ns berlin.models.data
  (:require 
   [clj-time.local :as ltime]
   [clj-time.core :as ctime]))


(def db (atom{}))
(declare all-todays-votes)

;;(defn addrecordedvote [doc]
;;  (swap! db  update-in ["recordedvotes"] conj doc))
;; (defn add [table doc] (swap! my-db update-in [table] conj doc))

(defn add
  ([table doc]
   (swap! db update-in [table] conj doc))
 ;; ([table doc person]
 ;;  (let [newvotes (conj (filter #(not ( = person (:person %))) (get @db table)) doc )]
 ;;    (swap! db update-in [table] (assoc @db table newvotes))     
 ;;  (swap! db update-in [table] (map #(if (= person (:person %))(assoc % :shop (:shop doc)) %)
                                    ;;(all-todays-votes)
  )
;;   )))
;; change this to a map using change-vote

(def today
  (str (ctime/day (ltime/local-now)) "-" (ctime/month (ltime/local-now)) "-" (ctime/year (ltime/local-now))))

(def shoplist ["BeanBlue" "Great Space" "Cumulus" "Axil"
               "Great Space" "Great Space" "Cumulus" "Axil"
               "BeanBlue" "Cumulus" "Axil" "Bean Republic"
               "Great Space" "Axil" "Cumulus" "McDonalds"
               ])

(def penguinlist ["Great Space" "Axil" "Cumulus" "Bean Republic"])


;;(def change-vote [person shop doc]
 ;; (if
 ;;     (= person (:person doc))
 ;;   (swap! doc :shop shop)))
    
 
(defn all-todays-votes [] 
    (filter #(= today (:day %)) (get @db "votes")))

(defn todaysresult []
  (:shop (first (filter #(= today (:day %)) (get @db "recordedvotes")))))

(defn record-votes []
  (if (and
       (nil? (todaysresult))
       (not (empty? (all-todays-votes))))
    (add "recordedvotes" {:day today :shop (rand-nth (map :shop (all-todays-votes)))})
 ))

(defn results []
  (if (empty? (todaysresult))
    (all-todays-votes)
    (todaysresult)))

(defn read-votes []
  (if (empty? (todaysresult))
    (all-todays-votes)
    (todaysresult)
    ))

(defn hasUserVotedToday? [person]
  (not (empty? (filter #(= person (:person %)) (all-todays-votes))))
  )
  
(defn add-vote [person shop]
  (if
      (not (hasUserVotedToday? person))
    ;; (add "votes" {:person person :shop shop :day today} person)
    (add "votes" {:person person :shop shop :day today})))

(defn chaosvote [person shop]
  (add-vote "ChaosMonkey" (rand-nth shoplist))
  (add-vote "ChaosPenguin" (rand-nth penguinlist))
  (add-vote person shop))
  

(defn add-newvote [person shop]
  (if (empty? (todaysresult))
    (chaosvote person shop)  ))

